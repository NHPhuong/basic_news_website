var express = require("express");
var router = express.Router();
var userMd = require("../models/user");

router.get("/", function(req, res){
	res.json({'message': 'This is admin page'});
})

router.get("/signup", function(req, res){
	res.render("signup", {data: {}});
});
router.post("/signup", function(req, res){
	var user = req.body;

	if(user.email.trim().length == 0){
			res.render("signup", {data: {error: "Email is required"}});
	}

	if(user.passwd != user.repasswd && user.passwd.trim().length != 0){
			res.render("signup", {data: {error: "Password not match"}});
	}

	//Insert to db
	user = {
		email: user.email,
		password: user.passwd,
		first_name: user.firstname,
		last_name: user.lastname
	};
	var result = userMd.addUser(user);
	if(!result){
		res.render("signup", {data: {error: "Could not insert user data"}});
	}
	else{
		res.json({message: "Insert sucessfully"});
	}

});

module.exports = router;