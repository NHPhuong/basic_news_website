var express = require("express");
var app = express();
//var config = require("config");
var bodyParser = require('body-parser');
app.use(bodyParser.json());	
app.use(bodyParser.urlencoded({extended: true}));

app.set('views', __dirname + '/apps/views');
app.set("view engine", "ejs");
app.use("/static", express.static(__dirname + "/public"));

var controllers = require(__dirname + "/apps/controllers");

app.use(controllers);

//var host = config.get('server.host');
//var port = config.get('server.port');
app.listen(3000, function(){
	console.log("Server is running on port", 3000);
});

